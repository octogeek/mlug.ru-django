SETUP

Clone repository somewhere

Install those (current version on mlug.ru site):
	* Django (1.8.4)
	* django-compresshtml (0.1.1)
	* django-tinymce (2.0.5)
	* django-flat-theme (1.1.1)

Edit either local_settings or server_settings and then exclude it from git index


Example for common local testing setup:

	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.sqlite3',
			'NAME': os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'db.sqlite3'),
		}
	}
	SECRET_KEY = 'secret'

	ADMINS = (('You', 'your@e.mail'),)
	SERVER_EMAIL = 'localhost'

	STATICFILES_DIRS = (
		os.path.dirname(os.path.dirname(__file__)) + '/static-link',
	)

	DEBUG = True
	CSRF_COOKIE_SECURE = False
	SESSION_COOKIE_SECURE = False
	EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
	LOCAL = True

Either configure uwsgi service or just run "python manage.py runserver ip.ad.re.ss:port"


CODE STYLING

Do not use spaces for indenting, use spaces for aligning
Do not use space to separate brackets in calls, use space to separate brackets in declarations
Use single line for beautification, use two to separate includes and main declarations
Always use "from django.utils.translation import ugettext as _" and "{% load i18n %}", do not leave untranslatable text unless it must be untranslatable
Line length is 200, 80 is for weaklings: it´s fine to write your code in shorter lines but do not needlessly rewrite other´s code
Write your code in a way that is readable to other people
Ask Goury Gabriev for help if not sure how to style your code or if you have a proposal


USAGE

Generate qr-tokens for polls:
./manage.py generate_tokens --token_count <how-many> --redirect <where-to-redirect> --urls | ./generate_qr_codes.bash /home/whatever/without/tailing/slash

Send email messages:
./manage.py sendmessages [--silent]
Add this command to your crontab or whatever else you use
