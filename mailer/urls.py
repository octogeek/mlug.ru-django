from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^$', 'mailer.views.index', name='index'),
	url(r'unsubscribe$', 'mailer.views.unsubscribe', name='unsubscribe'),
	url(r'subscribe$', 'mailer.views.subscribe', name='subscribe'),
	url(r'confirm$', 'mailer.views.confirm', name='confirm'),
	url(r'manage$', 'mailer.views.manage', name='manage'),
)
