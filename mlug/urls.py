from django.conf.urls import include, url
from django.contrib import admin
from cms import urls as cms_urls
from mailer import urls as mailer_urls
from polls import urls as polls_urls


urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^translator/', include('django.conf.urls.i18n')),
	url(r'^tinymce/', include('tinymce.urls')),
	url(r'^mailer/', include(mailer_urls)),
	url(r'^polls/', include(polls_urls)),
	url(r'^cms', include(cms_urls)),
	url(r'404$', 'mlug.views.not_found', name='404'),
	url(r'robots.txt$', 'mlug.views.robots', name='robots'),
	url(r'^$', 'cms.views.index', name='index'),
]
