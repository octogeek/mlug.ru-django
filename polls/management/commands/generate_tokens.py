from django.core.management.base import BaseCommand, CommandError
from polls.models import Token
from django.conf import settings
from polls.utils import token_url_for_email
import urllib


class Command (BaseCommand):
	help = 'Generate tokens with bulk purpose. Use --nosave if you dont want to save tokens to database and just testing.'

	def add_arguments (self, parser):
		parser.add_argument('--token_count',
			dest='token_count',
			default='1',
			help='How many tokens you want to make')
		parser.add_argument('--redirect',
			dest='redirect',
			default='1',
			help='Where to redirect after activation')
		parser.add_argument('--nosave',
			action='store_true',
			dest='nosave',
			default=False,
			help='Do not save anything to database')
		parser.add_argument('--urls',
			action='store_true',
			dest='urls',
			default=False,
			help='Output just urls, nothing else')

	def handle (self, *args, **options):
		token_count = int(options['token_count']) if options['token_count'].isdigit() else 1
		if not options['urls']:
			self.stdout.write('Making ' + str(token_count) + ' tokens')

		while token_count > 0:
			if not options['nosave']:
				token = Token.objects.create(purpose="bulk command")
				self.stdout.write(' https://' + settings.SITE_DOMAIN + '/polls/activate_token?token=' + str(token.uuid) + '&redirect_url=' + urllib.quote(options['redirect']))
			else:
				self.stdout.write(' https://' + settings.SITE_DOMAIN + '/polls/activate_token?token=' + str(Token(purpose="bulk command")) + '&redirect_url=' + urllib.quote(options['redirect']))
			token_count -= 1

		if not options['urls']:
			self.stdout.write('All requested tokens generated')
