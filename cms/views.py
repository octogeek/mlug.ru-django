# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _
from utils import get_next_2nd_friday_or_last_saturday, get_hooks
from models import Page
from hook_templates import YANDEX_MONEY_FORM
from mailer.utils import get_subsrcibe_form_html
from polls.utils import get_poll_form_html, get_poll_results_html


def hooks (hook, request):
	if hook == 'next_2nd_friday_or_last_saturday':
		return get_next_2nd_friday_or_last_saturday(request.LANGUAGE_CODE, '%A, %-d %b %Y')
	if hook == 'yandex_money_form':
		return YANDEX_MONEY_FORM
	if hook == 'subscribe_form':
		return get_subsrcibe_form_html(request)
	if 'poll_form|' in hook:
		if hook.count('|') < 2:
			return 'misconfigured pool form hook'
		data = hook.split('|')
		return get_poll_form_html(request, data[1].replace('-', ''), data[2])
	if 'poll_results|' in hook:
		if hook.count('|') < 1:
			return 'misconfigured pool results hook'
		data = hook.split('|')
		return get_poll_results_html(data[1].replace('-', ''))

	return str(hook)


def index (request):
	return page_by_alias(request, alias='index')


def page_by_alias(request, alias):
	if alias == '' or request.path == '/cms/index':
		return redirect("/")
	page = get_object_or_404(Page, alias=alias)
	if not page.is_published:
		raise Http404(_(u'You shall not pass'))
	header_includes = page.header_includes.split('\r\n') if page.header_includes else []
	footer_includes = page.footer_includes.split('\r\n') if page.footer_includes else []
	content = page.content
	for hook in get_hooks(content):
		hook_name = hook.replace('{{ ', '').replace(' }}', '').replace('{{', '').replace('}}', '')
		content = content.replace(hook, hooks(hook_name, request))
	return render(request, "cms/page.html", {'title': page.title, 'alias': alias, 'content': content, 'header_includes': header_includes, 'footer_includes': footer_includes})
