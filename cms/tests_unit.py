# -*- coding: utf-8 -*-
from django.test import TestCase
from django.http import HttpRequest, Http404
from models import Page
from django.contrib.auth.models import User
import views, utils
import datetime


class CmsTests (TestCase):
	def test_models_page (self):
		user = User.objects.create(username='meow')
		page = Page.objects.create(
			title = 'meow',
			alias = 'meow',
			content = 'meow',
			author = user,
		)
		page.is_published = True
		page.is_in_menu = True
		page.save()
		page = Page.objects.get(alias = 'meow')
		self.assertEqual(page.is_published, True)
		self.assertEqual(page.is_in_menu, True)

	def test_views_hooks (self):
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.META = {'CSRF_COOKIE': 'meow'}
		self.assertEqual(type(views.hooks('next_2nd_friday_or_last_saturday', request)), unicode)
		self.assertIn('<form ', views.hooks('yandex_money_form', request))
		self.assertIn('<form ', views.hooks('poll_form|meow|meow', request))
		self.assertIn('<form ', views.hooks('subscribe_form', request))
		self.assertFalse('<form ' in views.hooks('poll_form|meow', request))
		self.assertFalse('<form ' in views.hooks('poll_form', request))
		self.assertIn('<form ', views.hooks('poll_results|meow', request))
		self.assertFalse('<form ' in views.hooks('poll_results', request))
		self.assertIn('random invalid hook', views.hooks('random invalid hook', request))

	def test_views_index (self):
		user = User.objects.create(username='meow')
		page = Page.objects.create(alias='index', is_published = True, author=user)
		request = HttpRequest()
		response = views.index(request)
		self.assertEqual(response.status_code, 200)

	def test_views_page_by_alias (self):
		user = User.objects.create(username='meow')
		page = Page.objects.create(alias='meow_1', is_published = True, author=user)
		page = Page.objects.create(alias='meow_2', is_published = False, author=user)
		request = HttpRequest()
		response = views.page_by_alias(request, 'meow_1')
		self.assertEqual(response.status_code, 200)
		try: response = views.page_by_alias(request, 'meow_2')
		except Http404: pass
		try: response = views.page_by_alias(request, 'meow_3')
		except Http404: pass

	def test_utils_week_of_month (self):
		self.assertEqual(type(utils.week_of_month(datetime.datetime.now())), int)

	def test_utils_nth_weekday (self):
		self.assertEqual(type(utils.nth_weekday(datetime.datetime.now(), 2, 3)), datetime.datetime)

	def test_utils_get_next_2nd_friday_or_last_saturday (self):
		self.assertEqual(type(utils.get_next_2nd_friday_or_last_saturday('ru', '%A, %-d %b %Y')), unicode)
		self.assertEqual(type(utils.get_next_2nd_friday_or_last_saturday('en', '%A, %-d %b %Y')), unicode)

	def test_utils_get_hooks (self):
		self.assertEqual(
			utils.get_hooks(
				'{{meow}} {{ meow }}{{ moew}} moew {{meow }} { { m e o w } }'
			),
			['{{meow}}', '{{ meow }}', '{{ moew}}', '{{meow }}']
		)
