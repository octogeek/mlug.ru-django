from django.db import models
from tinymce import models as tinymce_models
import datetime
import uuid

HTML = '<p></p>'


class Subscriber (models.Model):
	email                       = models.EmailField()
	name                        = models.CharField(max_length=254, default='', blank=True)
	gender                      = models.CharField(max_length=2, choices=(("M", "Male"), ("F", "Female"), ("X", "Genderless")), default="X")
	date_subscribed             = models.DateTimeField(auto_now_add=True)
	date_confirmed              = models.DateTimeField(null=True, blank=True)
	date_lastmailed             = models.DateTimeField(null=True, blank=True)
	date_unsubscribed           = models.DateTimeField(null=True, blank=True)
	list_of_lists               = models.TextField(default="|")
	config_send_html            = models.BooleanField(default=True)
	config_mail_delay_interval  = models.DurationField(default=datetime.timedelta(days=0))
	config_language             = models.CharField(max_length=4, choices=(("ru", "Russian"), ("en", "English")), default="ru")
	config_access_token         = models.CharField(max_length=200, default=None, blank=False, null=True)
	def __unicode__ (self):
		return self.email


class List (models.Model):
	name                        = models.CharField(max_length=254)
	public                      = models.BooleanField(default=False)
	default                     = models.BooleanField(default=False)
	description_ru              = models.CharField(max_length=254, default="")
	description_en              = models.CharField(max_length=254, default="")
	uuid                        = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	def __unicode__ (self):
		return self.name


class Message (models.Model):
	list                        = models.ForeignKey(List)
	sent_to                     = models.ManyToManyField(Subscriber, db_table="mailer_tracker", blank=True)
	title                       = models.CharField(max_length=254, default="")
	title_ru                    = models.CharField(max_length=254, default="")
	title_en                    = models.CharField(max_length=254, default="")
	urgent                      = models.BooleanField(default=False)
	message_text_ru             = models.TextField(default="")
	message_text_en             = models.TextField(default="")
	message_html_ru             = tinymce_models.HTMLField(default=HTML)
	message_html_en             = tinymce_models.HTMLField(default=HTML)
	date_created                = models.DateTimeField(auto_now=True)
	ready                       = models.BooleanField(default=False)
	archived                    = models.BooleanField(default=False)
	def __unicode__ (self):
		return self.list.name + ' | ' + self.title
