#!/bin/bash

page=1
image=1
while read -r line; do
	qrencode -o $1/qrcode_$image.png -s 20 $line
	montage -label $line $1/qrcode_$image.png \
	-font /usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-C.ttf \
	-pointsize 18 -background White -geometry 980x1005 $1/qrcode_$image.png
	if [ $image == 24 ]
	then
		montage \
                $1/qrcode_1.png  $1/qrcode_2.png  $1/qrcode_3.png  $1/qrcode_4.png  $1/qrcode_5.png  $1/qrcode_6.png \
                $1/qrcode_7.png  $1/qrcode_8.png  $1/qrcode_9.png  $1/qrcode_10.png $1/qrcode_11.png $1/qrcode_12.png \
                $1/qrcode_13.png $1/qrcode_14.png $1/qrcode_15.png $1/qrcode_16.png $1/qrcode_17.png $1/qrcode_18.png \
                $1/qrcode_19.png $1/qrcode_20.png $1/qrcode_21.png $1/qrcode_22.png $1/qrcode_23.png $1/qrcode_24.png \
		-tile 4x6 -geometry +50+50 $1/qr_page$page.png
		let "page+=1"
		let "image=0"
	fi
	let "image+=1"
done

cd $1
rm $1/qrcode_1.png  $1/qrcode_2.png  $1/qrcode_3.png  $1/qrcode_4.png  $1/qrcode_5.png  $1/qrcode_6.png \
   $1/qrcode_7.png  $1/qrcode_8.png  $1/qrcode_9.png  $1/qrcode_10.png $1/qrcode_11.png $1/qrcode_12.png \
   $1/qrcode_13.png $1/qrcode_14.png $1/qrcode_15.png $1/qrcode_16.png $1/qrcode_17.png $1/qrcode_18.png \
   $1/qrcode_19.png $1/qrcode_20.png $1/qrcode_21.png $1/qrcode_22.png $1/qrcode_23.png $1/qrcode_24.png

tar -cvf qrcodes.tar.gz *.png --remove-files
