# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from django.contrib.auth.models import User
from models import Subscriber, List, Message
from django.core.management import call_command
import views

c = Client(enforce_csrf_checks=True)


class MailerTests (TestCase):
	def test_index_render (self):
		"""
		Can I render index page?
		"""
		response = c.get('/mailer/')
		self.assertEqual(response.status_code, 200)

	def test_subscribe_render_405 (self):
		"""
		Can I get 405 error from subscribe page?
		"""
		response = c.get('/mailer/subscribe')
		self.assertEqual(response.status_code, 405)

	def test_subscribe_bad_email_redirect (self):
		"""
		Can I get redirected by posting bad email?
		"""
		response_1 = c.get('/mailer/')
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow'})
		self.assertEqual(response_2.status_code, 302)

	def test_subscribe_good_render (self):
		"""
		Can I get success page render?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)

	def test_subscribe_then_confirm_then_unsubscribe_then_resubscribe (self):
		"""
		Can I subscribe, confirm, unsubscribe, resubscribe?
		"""
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.get('/mailer/unsubscribe?token=' + subscriber.config_access_token)
		self.assertEqual(response_5.status_code, 200)
		response_6 = c.post('/mailer/unsubscribe?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token})
		self.assertEqual(response_6.status_code, 302)
		response_7 = c.get('/mailer/')
		self.assertEqual(response_7.status_code, 200)
		response_8 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_8.status_code, 302) # redirect 302 means email is not accepted

	def test_subscribe_then_set_delay_to_zero_then_get_two_messages (self):
		"""
		Can I subscribe, set delay to zero and get two messages?
		"""
		list = List.objects.create(
			name = u'news',
			public = True,
			default = True,
			description_ru = u'новости',
			description_en = u'news',
		)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '0', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response_5.status_code, 200)
		message_1 = Message.objects.create(
			list                        = list,
			title                       = u'meow 1',
			title_ru                    = u'meow 1',
			title_en                    = u'meow 1',
			message_text_ru             = u'meow 1',
			message_text_en             = u'meow 1',
			message_html_ru             = u'meow 1',
			message_html_en             = u'meow 1',
		)
		message_2 = Message.objects.create(
			list                        = list,
			title                       = u'meow 2',
			title_ru                    = u'meow 2',
			title_en                    = u'meow 2',
			message_text_ru             = u'meow 2',
			message_text_en             = u'meow 2',
			message_html_ru             = u'meow 2',
			message_html_en             = u'meow 2',
		)
		message_1.ready = True
		message_1.save()
		output_1 = call_command('sendmessages', '--silent', '--fake')
		message_2.ready = True
		message_2.save()
		output_1 = call_command('sendmessages', '--silent', '--fake')
		message_1 = Message.objects.get(title=u'meow 1')
		message_2 = Message.objects.get(title=u'meow 2')
		self.assertEqual(message_1.sent_to.all()[0], subscriber)
		self.assertEqual(message_2.sent_to.all()[0], subscriber)
		self.assertEqual(message_1.archived, True)
		self.assertEqual(message_2.archived, True)

	def test_subscribe_to_private_list (self):
		"""
		Can I subscribe to private list?
		"""
		list = List.objects.create(
			name = u'news',
			public = False,
			default = False,
			description_ru = u'новости',
			description_en = u'news',
		)
		response_1 = c.get('/mailer/')
		self.assertEqual(response_1.status_code, 200)
		response_2 = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		self.assertEqual(response_2.status_code, 200)
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response_3 = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		self.assertEqual(response_3.status_code, 200)
		response_4 = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		self.assertEqual(response_4.status_code, 302)
		response_5 = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '1', 'list_'+str(list.uuid): 'yes'})
		self.assertEqual(response_5.status_code, 200)
		self.assertEqual(str(list.uuid) in subscriber.list_of_lists, False)

	def test_send_two_normal_messages_and_one_urgent (self):
		"""
		Can I get two normal messages and one urgent?
		"""
		list = List.objects.create(
			name = u'news',
			public = True,
			default = True,
			description_ru = u'новости',
			description_en = u'news',
		)
		response = c.get('/mailer/')
		response = c.post('/mailer/subscribe', {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'email': 'meow@meow.me'})
		subscriber = Subscriber.objects.get(email='meow@meow.me')
		response = c.get('/mailer/manage?token=' + subscriber.config_access_token)
		response = c.get('/mailer/confirm?token=' + subscriber.config_access_token)
		response = c.post('/mailer/manage?token=' + subscriber.config_access_token, {'csrfmiddlewaretoken': c.cookies['csrftoken'].value, 'token': subscriber.config_access_token, 'config_mail_delay_interval': '1', 'list_'+str(list.uuid): 'yes'})
		message = Message.objects.create(
			list                        = list,
			title                       = u'meow 1',
			title_ru                    = u'meow',
			title_en                    = u'meow',
			message_text_ru             = u'meow',
			message_text_en             = u'meow',
			message_html_ru             = u'meow',
			message_html_en             = u'meow',
		)
		message.ready = True
		message.save()
		output = call_command('sendmessages', '--silent', '--fake')
		message = Message.objects.get(title=u'meow 1')
		self.assertEqual(message.sent_to.all()[0], subscriber)
		self.assertEqual(message.archived, True)
		message = Message.objects.create(
			list                        = list,
			title                       = u'meow 2',
			title_ru                    = u'meow',
			title_en                    = u'meow',
			message_text_ru             = u'meow',
			message_text_en             = u'meow',
			message_html_ru             = u'meow',
			message_html_en             = u'meow',
		)
		message.ready = True
		message.urgent = True
		message.save()
		output = call_command('sendmessages', '--silent', '--fake')
		message = Message.objects.get(title=u'meow 2')
		self.assertEqual(message.sent_to.all()[0], subscriber)
		self.assertEqual(message.archived, True)
		message = Message.objects.create(
			list                        = list,
			title                       = u'meow 3',
			title_ru                    = u'meow',
			title_en                    = u'meow',
			message_text_ru             = u'meow',
			message_text_en             = u'meow',
			message_html_ru             = u'meow',
			message_html_en             = u'meow',
		)
		message.ready = True
		message.save()
		output = call_command('sendmessages', '--silent', '--fake')
		message = Message.objects.get(title=u'meow 3')
		self.assertEqual(subscriber in message.sent_to.all(), False)
		self.assertEqual(message.archived, False)
