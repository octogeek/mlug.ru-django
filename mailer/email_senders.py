# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from django.conf import settings
from .settings import APP_NAME, APP_URL


def subscribe (subscriber):
	send_mail(
		u'[' + _(settings.SITE_NAME) + u'] ' + _(APP_NAME) + u' ' + _(u'subscription confirmation'),
		_(u'I want to confirm your will to subscribe %(app_name)s\n\n'
			+ u'Please click/open/follow this link for your subscription management and confirm your will:\n'
			+ u'https://%(site_domain)s/%(app_url)s/manage?token=%(token)s\n'
			+ u'This link have no expiration date and should be valid unless you change your token somehow.\n'
			+ u'Do not share this link.\n\n'
			+ u'Your email address should be: %(email)s'
		) % {
			'site_domain': settings.SITE_DOMAIN,
			'app_name': APP_NAME,
			'app_url': APP_URL,
			'token': subscriber.config_access_token,
			'email': subscriber.email,
		},
		settings.DEFAULT_FROM_EMAIL,
		[subscriber.email],
		fail_silently=True
	)
	return True


def resubscribe (subscriber):
	send_mail(
		u'[' + _(settings.SITE_NAME) + u'] ' + _(APP_NAME) + u' ' + _(u'subscription token request'),
		_(u'Somebody just tried to resubscribe you for %(app_name)s´s mailing list.\n\n'
			+ u'This is link for your subscription management:\n'
			+ u'https://%(site_domain)s/%(app_url)s/manage?token=%(token)s\n\n'
			+ u'Your email address should be: %(email)s'
		) % {
			'site_domain': settings.SITE_DOMAIN,
			'app_name': APP_NAME,
			'app_url': APP_URL,
			'token': subscriber.config_access_token,
			'email': subscriber.email,
		},
		settings.DEFAULT_FROM_EMAIL,
		[subscriber.email],
		fail_silently=True
	)
	return True
