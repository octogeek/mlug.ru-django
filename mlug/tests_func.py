from django.test import TestCase, Client
import views

c = Client()


class MlugTests (TestCase):
	def test_404_render (self):
		"""
		Can I render 404 page?
		"""
		response = c.get('/404')
		self.assertEqual(response.status_code, 404)

	def test_robots (self):
		"""
		Can I get robots.txt?
		"""
		response = c.get('/robots.txt')
		self.assertEqual(response.status_code, 200)
