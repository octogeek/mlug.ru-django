# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.conf import settings

YANDEX_MONEY_FORM = (
	'<form class="smallform" action="https://money.yandex.ru/quickpay/confirm.xml" method="post">'
	+ '<input type="hidden" name="receiver" value="41001386720615">'
	+ '<input type="hidden" name="short-dest" value="' + _(u'Donation for') + ' ' + settings.SITE_NAME + '">'
	+ '<input type="hidden" name="quickpay-form" value="donate">'
	+ '<input type="hidden" name="label" value="' + settings.SITE_DOMAIN + '">'
	+ '<input type="hidden" name="formcomment" value="' + settings.SITE_NAME + ' donation">'
	+ '<input type="hidden" name="referer" value="https://' + settings.SITE_DOMAIN + '">'
	+ '<input type="hidden" name="targets" value="' + _(u'Site support') + '">'
	+ '<input type="hidden" name="successURL" value="https://' + settings.SITE_DOMAIN + '">'
	+ '<input type="hidden" name="is-inner-form" value="true">'
	+ '<input type="hidden" name="writable-sum" value="true">'
	+ '<input type="hidden" name="need-email" value="true">'
	+ '<p>'
	+ '<input type="radio" name="paymentType" value="PC">' + _(u'Yandex Money') + '<br />'
	+ '<input type="radio" name="paymentType" value="AC" checked="checked">' + _(u'Bank card')
	+ '</p>'
	+ '<span class="formlabel">' + _(u'How much') + ': </span><input class="smallinput" type="text" name="sum" value="500"> ' + _(u'Rubles') + '<br />'
	+ '<input class="longinput" type="text" name="comment" value="" placeholder="' + _(u'Comment') + '"><br />'
	+ '<input type="submit" value="' + _(u'Donate') + '">'
	+ '</form>'
)
