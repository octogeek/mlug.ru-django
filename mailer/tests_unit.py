# -*- coding: utf-8 -*-
from django.test import TestCase
from django.http import HttpRequest
from models import Subscriber, List, Message
from django.contrib.auth.models import User
import views, utils, email_senders
from management.commands import sendmessages

class MailerTests (TestCase):
	def test_models_subscriber (self):
		subscriber = Subscriber.objects.create(
			email = "m@e.ow",
			config_access_token = "meow",
		)
		subscriber.name = "meow"
		subscriber.list_of_lists = "|meow|"
		subscriber.save()
		subscriber = Subscriber.objects.get(email="m@e.ow")
		self.assertEqual(subscriber.name, "meow")
		self.assertEqual(subscriber.list_of_lists, "|meow|")

	def test_models_list (self):
		list = List.objects.create(name="meow")
		uuid = list.uuid
		list.save()
		list = List.objects.get(uuid=uuid)
		self.assertEqual(list.name, "meow")

	def test_models_message (self):
		list = List.objects.create(name="meow")
		message = Message.objects.create(list=list)
		message.title = "meow"
		message.title_en = "Meowing message"
		message.title_ru = "Мяу"
		message.save()
		message = Message.objects.get(title="meow")
		self.assertEqual(message.title_en, "Meowing message")
		self.assertEqual(message.title_ru, u"Мяу")

	def test_views_index (self):
		request = HttpRequest()
		response = views.index(request)
		self.assertIn("<form ", response.content)
		request.GET['bad_email'] = 'yes'
		request.LANGUAGE_CODE = 'en'
		response = views.index(request)
		self.assertIn("<p>", response.content)

	# other views tests is not viable here and done in tests_func

	def test_utils_validate_email (self):
		self.assertFalse(utils.validate_email("meow"))
		self.assertFalse(utils.validate_email("me@ow"))
		self.assertFalse(utils.validate_email("me@o.w"))
		self.assertFalse(utils.validate_email("m@e@o.ww"))
		self.assertFalse(utils.validate_email("m.e.ow"))
		self.assertTrue(utils.validate_email("m@e.ow"))
		self.assertTrue(utils.validate_email("meow@meow.meow"))

	def test_utils_get_list_of_lists (self):
		list1 = List.objects.create(name="meow1", public=True)
		list2 = List.objects.create(name="meow2", public=True)
		list3 = List.objects.create(name="meow3", public=True)
		self.assertEqual(
			utils.get_list_of_lists(),
			[
				(list1.name, list1.uuid, list1.default),
				(list2.name, list2.uuid, list2.default),
				(list3.name, list3.uuid, list3.default),
			]
		)

	def test_utls_get_subsrcibe_form_html (self):
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.META = {'CSRF_COOKIE': 'meow'}
		result = utils.get_subsrcibe_form_html(request)
		self.assertIn("<form ", result)

	def test_email_senders_subscribe (self):
		subscriber = Subscriber.objects.create(
			email = "m@e.ow",
			config_access_token = "meow",
		)
		self.assertTrue(email_senders.subscribe(subscriber))

	def test_email_senders_resubscribe (self):
		subscriber = Subscriber.objects.create(
			email = "m@e.ow",
			config_access_token = "meow",
		)
		self.assertTrue(email_senders.resubscribe(subscriber))

	def test_sendmessages_hooks (self):
		self.assertIn('https://', sendmessages.hooks('{{ token_url_for_email|/ }}'))

	# sendmessages command testing is not viable here and done in tests_func
