from django.http import Http404
from django.shortcuts import render
from django.utils.translation import ugettext as _


def not_found (request):
	raise Http404(_(u'Page does not exist'))


def robots (request):
	return render(request, 'robots.txt')
