from django.http import Http404, HttpResponse
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from django.utils import translation
from django.utils.translation import ugettext as _
from django.utils.crypto import get_random_string
from django.utils.html import strip_tags
from django.contrib.auth.models import User, Permission
from utils import validate_email, get_list_of_lists
import email_senders
from models import Subscriber, List, Message
import django.utils.timezone
import datetime
from django.conf import settings
from settings import APP_NAME, APP_URL
import re
uuid4hex = re.compile('[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}\Z', re.I)


def index (request):
	message = ''
#	if request.user.is_authenticated() and request.user.userprofile.validation_email_valid:
#		email = request.user.email
#	else:
#		email = ''
	email = ''
	lists = get_list_of_lists()
	if request.GET.get('bad_email') == 'yes':
		message = _('I dont like this email')
	return render(request, 'mailer/index.html', {'lists': lists, 'email': email, 'message': message, 'app_name': APP_NAME, 'app_url': APP_URL,})


def subscribe (request):
	if not request.POST.get('csrfmiddlewaretoken'):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	elif not validate_email(request.POST.get('email')):
		return redirect('/'+APP_URL+'/?bad_email=yes')
	elif Subscriber.objects.filter(email=request.POST.get('email')):
		#resend token
		subscriber = Subscriber.objects.get(email=request.POST.get('email'))
		if subscriber.date_unsubscribed:
			return redirect('/'+APP_URL+'/?bad_email=yes')
		else:
			email_senders.resubscribe(subscriber)
			return render(request, 'mailer/success.html', {})
	else:
		#perform subscribe
		list_of_lists = '|'
		for list_id in request.POST.items():
			if list_id[0][0:5] == 'list_':
				list_of_lists += list_id[0].replace('list_', '', 1) + '|'
		subscriber = Subscriber.objects.create(email=request.POST.get('email'))
		subscriber.list_of_lists = list_of_lists
		subscriber.config_access_token = str(subscriber.id) + get_random_string(length=64)
		subscriber.save()
		email_senders.subscribe(subscriber)
		return render(request, 'mailer/success.html', {})


def confirm (request):
	if (not request.GET.get('token')) or (not Subscriber.objects.filter(config_access_token=request.GET.get('token'))):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	subscriber = Subscriber.objects.get(config_access_token=request.GET.get('token'))
	if not subscriber.date_confirmed:
		subscriber.date_confirmed = django.utils.timezone.now()
		subscriber.save()
	return redirect('/'+APP_URL+'/manage?token=' + subscriber.config_access_token)


def unsubscribe (request):
	if (not request.GET.get('token')) or (not Subscriber.objects.filter(config_access_token=request.GET.get('token'))):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	if not request.POST.get('token'):
		return render(request, 'mailer/unsubscribe.html', {'token': request.GET.get('token'), 'app_name': APP_NAME, 'app_url': APP_URL,})
	subscriber = Subscriber.objects.get(config_access_token=request.GET.get('token'))
	subscriber.date_unsubscribed = django.utils.timezone.now()
	subscriber.save()
	return redirect('/'+APP_URL+'/manage?token=' + subscriber.config_access_token)


def manage (request):
	if (not request.GET.get('token')) or (not Subscriber.objects.filter(config_access_token=request.GET.get('token'))):
		return render(request, '404.html', {'message': _('You should not access this page directly')}, status=405)
	if request.POST.get('csrfmiddlewaretoken'):
		update(request)
	subscriber = Subscriber.objects.get(config_access_token=request.GET.get('token'))
	list_of_lists = [list for list in subscriber.list_of_lists.split('|') if list != '']
	lists = []
	for list in List.objects.all():
		if list.public:
			active = list_of_lists.count(str(list.uuid))
			lists.append((list.name, list.uuid, active))
		elif list_of_lists.count(str(list.uuid)):
			active = 1
			lists.append((list.name + u' [' +_(u'private list') + ']', list.uuid, active))
	return render(request, 'mailer/manage.html', {
		'token': request.GET.get('token'),
		'email': subscriber.email,
		'lists': lists,
		'config_language': subscriber.config_language,
		'config_mail_delay_interval': str(subscriber.config_mail_delay_interval.days),
		'config_send_html': subscriber.config_send_html,
		'name': subscriber.name,
		'gender': subscriber.gender,
		'date_confirmed': subscriber.date_confirmed,
		'date_unsubscribed': subscriber.date_unsubscribed,
		'updated': True if request.POST.get('csrfmiddlewaretoken') else False,
		'app_name': APP_NAME, 'app_url': APP_URL,
		#TODO cnahgeable token
	})


def update (request):
	subscribers = Subscriber.objects.filter(config_access_token=request.POST.get('token'))
	if subscribers: subscriber = subscribers[0]
	else: return False
	subscriber.config_language = request.POST.get('config_language') if (request.POST.get('config_language') in ['ru', 'en']) else 'ru'
	subscriber.gender = request.POST.get('gender') if (request.POST.get('gender') in ['F', 'M', 'X']) else 'X'
	subscriber.config_send_html = request.POST.get('config_send_html') if (type(request.POST.get('config_send_html')) == bool) else True
	subscriber.config_mail_delay_interval = datetime.timedelta(days=int(request.POST.get('config_mail_delay_interval'))) if request.POST.get('config_mail_delay_interval').isdigit() else datetime.timedelta(days=1)
	if subscriber.config_mail_delay_interval > datetime.timedelta(days=30):
		subscriber.config_mail_delay_interval = datetime.timedelta(days=30)
	subscriber.name = strip_tags(request.POST.get('name')) if request.POST.get('name') else ''
	list_of_lists = '|'
	for list_id in request.POST.items():
		if list_id[0][0:5] == 'list_':
			list_uuid = list_id[0].replace('list_', '', 1)
			if uuid4hex.match(list_uuid.replace('-', '')) and List.objects.filter(uuid=list_uuid):
				if List.objects.get(uuid=list_uuid).public or list_uuid in subscriber.list_of_lists:
					list_of_lists += list_id[0].replace('list_', '', 1) + '|'
	subscriber.list_of_lists = list_of_lists
	subscriber.save()
	return True
