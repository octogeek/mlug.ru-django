import os
from django.utils.translation import ugettext_lazy as _
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = False
ALLOWED_HOSTS = '*'

SITE_DOMAIN = 'mlug.ru'
SITE_NAME = u'Moscow GNU/linux user group'

INSTALLED_APPS = (
	'flat',
	'tinymce',
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'cms',
	'mailer',
	'polls',
)

MIDDLEWARE_CLASSES = (
	'compresshtml.middleware.CompressHtmlMiddleware',
	'mlug.utils.ForceDefaultLanguageMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'django.middleware.security.SecurityMiddleware',
	'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'mlug.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [BASE_DIR + '/templates',],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
				'django.core.context_processors.i18n',
				'polls.utils.count_cow_powers',
			],
		},
	},
]

LOCALE_PATHS = [
	os.path.join(BASE_DIR, 'locale'),
]

WSGI_APPLICATION = 'mlug.wsgi.application'

TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LANGUAGES = (
	('ru', _('Russian')),
	('en', _('English')),
)
LANGUAGE_CODE = 'ru'
LANGUAGE_COOKIE_NAME = '_language'

STATIC_URL = '/static/'
PROJECT_DIR = os.path.dirname(__file__)
STATIC_ROOT = os.path.join(PROJECT_DIR, '../static')

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

APPEND_SLASH = True

TINYMCE_COMPRESSOR = True
TINYMCE_DEFAULT_CONFIG = {
	'plugins': 'inlinepopups',
	'theme': 'advanced',
	'theme_advanced_buttons1': 'formatselect,|,bold,italic,underline,strikethrough,hr,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,outdent,indent,|,undo,redo,|,link,unlink,|,cleanup,removeformat,|,sub,sup,|,forecolor,backcolor,|,code',
	'theme_advanced_buttons2': '',
	'theme_advanced_buttons3': '',
	'forced_root_block': '',
	'force_br_newlines' : True,
	'force_p_newlines': False,
	'theme_advanced_blockformats': 'p,h1,h2,h3,h4,h5,h6,blockquote,code',
	'theme_advanced_source_editor_width': '800',
	'theme_advanced_source_editor_height': '600',
	'theme_advanced_statusbar_location': 'bottom',
	'theme_advanced_resizing': True,
	'relative_urls' : False,
	'remove_script_host' : False,
	'convert_urls': False,
	'keep_styles' : False,
	'width': '70%',
	'height': '200',
	'cleanup_on_startup': True,
	'custom_undo_redo_levels': 50,
}

LOCAL = False
from server_settings import *
from local_settings import *
