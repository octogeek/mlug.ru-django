from django.contrib import admin
from django.http import HttpResponseRedirect
from .models import Subscriber, List, Message


def ready_message(modeladmin, request, queryset):
	queryset.update(ready=True)
ready_message.short_description = "Ready selected messages"


class SubscriberAdmin (admin.ModelAdmin):
	list_display = ('email', 'date_confirmed', 'date_unsubscribed', 'config_language')
	ordering = ['-date_subscribed']


class ListAdmin (admin.ModelAdmin):
	fieldsets = [
		(None,           {'fields': ['name', 'public', 'default', 'description_en', 'description_ru']}),
#		('Extra tuning', {'fields': ['user', 'validation_email_sent'], 'classes': ['collapse']}),
	]
	list_display = ('name', 'public', 'default')


class MessageAdmin (admin.ModelAdmin):
	def changelist_view(self, request, extra_context=None):
		if not request.META['QUERY_STRING'] and \
		   not request.META.get('HTTP_REFERER', '').startswith(request.build_absolute_uri()):
			return HttpResponseRedirect(request.path + "?archived__exact=0")
		return super(MessageAdmin,self).changelist_view(request, extra_context=extra_context)

	fieldsets = [
		(None,           {'fields': ['list', 'urgent', 'title', 'title_ru', 'title_en']}),
		('Message',      {'fields': ['message_text_ru', 'message_html_ru', 'message_text_en', 'message_html_en'], 'classes': ['wide']}),
	]
	list_filter = (
		('ready', admin.BooleanFieldListFilter),
		('archived', admin.BooleanFieldListFilter),
	)
	list_display = ('title', 'list', 'urgent', 'date_created', 'ready', 'archived')
	ordering = ['-date_created']
	actions = [ready_message]


admin.site.register(Subscriber, SubscriberAdmin)
admin.site.register(List, ListAdmin)
admin.site.register(Message, MessageAdmin)
