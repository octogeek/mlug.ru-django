# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.utils import timezone
from mailer.models import Message, Subscriber, List
from mailer.settings import APP_URL
from django.conf import settings
import re
from polls.utils import token_url_for_email


def hooks (hook):
	if 'token_url_for_email|' in hook:
		if hook.count('|') < 1:
			raise Exception('misconfigured token hook')
		data = hook.split('|')
		return token_url_for_email( 'https://'+settings.SITE_DOMAIN+data[1] )

	raise Exception('misconfigured hook')


def sendmail (subscriber, message):
	title = u''
	body = u''
	html = None
	if subscriber.config_language == 'ru':
		title = message.title_ru
		body  = message.message_text_ru + u'\n\nСсылка чтобы настроить свою подписку или отписаться:\n'
		body += u'https://' + settings.SITE_DOMAIN + u'/' + APP_URL + u'/manage?token=' + subscriber.config_access_token + u'>'
		if subscriber.config_send_html:
			html  = u'<html><body>' + message.message_html_ru
			html += u'<p>Это рассылка от ' + settings.SITE_NAME + '<br/>'
			html += u'<a href="https://' + settings.SITE_DOMAIN + u'/' + APP_URL + u'/manage?token=' + subscriber.config_access_token + u'>'
			html += u'Ссылка чтобы настроить свою подписку или отписаться</a>.</p></body></html>'
	elif subscriber.config_language == 'en':
		title = message.title_en
		body = message.message_html_en + u'\n\nUrl for subscription management and unsubscription:\n'
		body += u'https://' + settings.SITE_DOMAIN + u'/' + APP_URL + u'/manage?token=' + subscriber.config_access_token + u'>'
		if subscriber.config_send_html:
			html = u'<html><body>' + message.message_html_en
			html += u'<p>This message is sent to you by ' + settings.SITE_NAME + '<br/>'
			html += u'<a href="https://' + settings.SITE_DOMAIN + u'/' + APP_URL + u'/manage?token=' + subscriber.config_access_token + u'>'
			html += u'Url for subscription management and unsubscription</a>.</p></body></html>'

	for hook in re.findall(r'\{{[^()]*\}}', body):
		hook_name = hook.replace('{{ ', '').replace(' }}', '').replace('{{', '').replace('}}', '')
		body = body.replace(hook, hooks(hook_name))
		if html:
			html = html.replace(hook, hooks(hook_name))

	send_mail(
		title,
		body,
		settings.DEFAULT_FROM_EMAIL,
		[subscriber.email],
		fail_silently=True,
		html_message=html
	)


class Command (BaseCommand):
	help = 'Send messages to subscribers. If you want to test it without really doing anything you should use both --fake and --nosave!'

	def add_arguments (self, parser):
		parser.add_argument('--fake',
			action='store_true',
			dest='fake',
			default=False,
			help='Only count, do not perform actual send')
		parser.add_argument('--nosave',
			action='store_true',
			dest='nosave',
			default=False,
			help='Do not save anything to database')
		parser.add_argument('--silent',
			action='store_true',
			dest='silent',
			default=False,
			help='Do not output as much messages')

	def handle (self, *args, **options):
		send = not options['fake']
		save = not options['nosave']
		silent = options['silent']
		task_start = timezone.now()
		mailed_subscribers = []
		subscribers = Subscriber.objects.filter(date_unsubscribed=None).exclude(date_confirmed=None)
		self.stdout.write('Got ' + str(len(subscribers)) + ' active subscribers')
		if not silent: self.stdout.write('')

		for message in Message.objects.filter(ready=True, archived=False):
			if not silent:
				self.stdout.write('Processing message [' + str(message) + ']:')
			else:
				self.stdout.write('[' + str(message) + ']:', ending='')
			uuid = str(message.list.uuid)
			do_not_archive_yet = False
			recipients = subscribers.filter(date_confirmed__lte=message.date_created)

			#go thru subscribers and send messages
			for recipient in recipients:
				if uuid in recipient.list_of_lists.split('|'):
					if not (recipient.date_lastmailed is None or recipient.date_lastmailed <= task_start - recipient.config_mail_delay_interval) and not message.urgent:
						# too soon to send a message
						if not silent: self.stdout.write('Recipient ' + recipient.email + ' is not ready yet (too soon since last message)')
						do_not_archive_yet = True
					elif recipient not in message.sent_to.all():
						# sending message
						mailed_subscribers.append(recipient)
						if not silent: self.stdout.write('Sending message to ' + recipient.email)
						if send: sendmail(recipient, message)
						if save: message.sent_to.add(recipient)
					else:
						# message was sent in past
						if not silent: self.stdout.write('Sending message to ' + recipient.email + ' in not required as it was already done')

			if do_not_archive_yet:
				if not silent:
					self.stdout.write('Message was not sent to every recipient thus will be resent later')
				else:
					self.stdout.write('')
			else:
				if not silent:
					self.stdout.write('Message was sent to every recipient and going to be archived')
				else:
					self.stdout.write(' [archived]')
				message.archived = True

			if save: message.save()
			if not silent: self.stdout.write('')

		# updade date_lastmailed for every mailed subscriber
		for subscriber in mailed_subscribers:
			subscriber.date_lastmailed = task_start
			if save: subscriber.save()

		self.stdout.write(str(len(mailed_subscribers)) + ' messages sent')
